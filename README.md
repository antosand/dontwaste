# Don’t waste
**Andrii Yermakov**
**Andrii Antosha**

## Cíl: Vytvořit aplikaci pro umožnění sdílení potravin a různých materiálních  věcí mezi přihlášenými uživateli.

Inspirace:
*  [Too Good To Go](https://toogoodtogo.co.uk/en-gb)
*  [OLIO - The Food Sharing Revolution](https://olioex.com/)

Funkce:  
*  Registrace / přihlášení 
*  Nastavení účtu (změna uživatelských údajů)
*  Přidání nabídky, kterou plánujete sdílet
*  Prohlížení detailu nabídky
*  Přidání poznámek (komentářů, hodnocení) k nabídkám 
*  Přidání fotek k nabídkám
*  Rezervace nabídky / zrušení rezervace
*  Vyhledávání nabídky (+ na mapě )
*  Nahlášení nevhodného obsahu
*  Použití mapy
*  Komunikace mezi uživatele
*  Filtrování nabídek podle kategorií
*  Prioritizace nabídek podle vzdálenosti

Pro role admin:
*  Mazání, banování, editace účtů
*  Mazání nabídek, komentářů
*  Přehled statistiky využití aplikace
