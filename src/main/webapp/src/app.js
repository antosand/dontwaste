import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Navbar from './components/Navbar';
import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import CreateOffer from './components/CreateOffer';

const App = () => {
    return (
        <BrowserRouter>
            <div className="App">
                <Navbar />
                <Switch>
                    <Route path='/signin' component={SignIn} />
                    <Route path='/signup' component={SignUp} />
                    <Route path='/create' component={CreateOffer} />
                </Switch>
            </div>
        </BrowserRouter>
    )
};

ReactDOM.render(<App/>, document.getElementById("app"));
