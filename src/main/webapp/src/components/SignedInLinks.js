import React from 'react';
import { NavLink } from 'react-router-dom';

const SignedInLinks = () => {
    return (
        <div>
            <ul className="right">
                <li><NavLink to='/create'>New Offer</NavLink></li>
                <li><NavLink to='/'>Log Out</NavLink></li>
                <li><NavLink to='/' className="btn btn-floating pink lighten-1">UU</NavLink></li>
            </ul>
        </div>
    )
}

export default SignedInLinks;