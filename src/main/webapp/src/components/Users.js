import React from 'react';

class Users extends React.Component {
    constructor(props) {
        super(props);

        this.getReact = this.getReact.bind(this);
    }

    getReact () {
        fetch('/api/hello').then(
            console.log("I got hello route yeah!")
        );
    }

    render() {
        return (
            <div>
                <h2>Get react route</h2>
                <button onClick={this.getReact}>GET</button>
            </div>
        )
    }
}

export default Users;