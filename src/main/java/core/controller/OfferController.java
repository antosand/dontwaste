package core.controller;

import core.controller.util.RestUtils;
import core.model.Comment;
import core.model.Offer;
import core.security.model.AuthenticationToken;
import core.service.CategoryService;
import core.service.CommentService;
import core.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.persistence.NoResultException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/offers")
public class OfferController {

    private final CategoryService service;
    private final OfferService offerService;
    private final CommentService commentService;

    @Autowired
    public OfferController(CategoryService service, OfferService offerService, CommentService commentService) {
        this.service = service;
        this.offerService = offerService;
        this.commentService = commentService;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_GUEST')")
    @GetMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Offer getOffer(@PathVariable("id") Long id) {
        final Offer offer = offerService.findOffer(id);
        if (offer == null) {
            throw new NoResultException("Offer with id " + id + " was not found.");
        }
        return offer;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createOffer(@RequestBody Offer offer, Principal principal) {
        final AuthenticationToken auth = (AuthenticationToken) principal;
        offer.setCreatedBy(auth.getPrincipal().getUser());
        offerService.addOffer(offer);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", offer.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @DeleteMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void removeOffer(@PathVariable("id") Long id) {
        final Offer toRemove = offerService.findOffer(id);
        if (toRemove == null) {
            return;
        }
        offerService.removeOffer(toRemove);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_GUEST')")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Offer> getOffers() {
        return offerService.findAll();
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @RequestMapping(value = "/{id}/comments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Comment> getComments(@PathVariable("id") Long offer) {
        return commentService.findAll(offer);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PostMapping(value = "/{id}/comments", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addComment(@RequestBody Comment comment, Principal principal, @PathVariable("id") Long offer) {
        final AuthenticationToken auth = (AuthenticationToken) principal;
        comment.setOffer(offerService.findOffer(offer));
        offerService.addComment(auth.getPrincipal().getUser(), comment);
    }
}