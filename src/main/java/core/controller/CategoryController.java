package core.controller;

import core.controller.util.RestUtils;
import core.model.Category;
import core.model.Offer;
import core.service.CategoryService;
import core.service.OfferService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.persistence.NoResultException;
import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private CategoryService categoryService;
    private OfferService offerService;

    public CategoryController(CategoryService categoryService, OfferService offerService) {
        this.categoryService = categoryService;
        this.offerService = offerService;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_GUEST')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Category> getCategories() {
        return categoryService.findAll();
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createCategory(@RequestBody Category category) {
        categoryService.createCategory(category);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", category.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void removeCategory(@PathVariable("id") Long id) {
        final Category toRemove = categoryService.findCategory(id);
        if (toRemove == null) {
            return;
        }
        categoryService.removeCategory(toRemove);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_GUEST')")
    @GetMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Category getCategoty(@PathVariable("id") Long id) {
        final Category category = categoryService.findCategory(id);
        if (category == null) {
            throw new NoResultException("Offer with id " + id + " was not found.");
        }
        return category;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_GUEST')")
    @GetMapping(value = "/{name}/offers", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<Offer> getOffersInCategory(@PathVariable("name") String name) {
        return categoryService.findAllOffersByCategory(name);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PostMapping(value = "/{id}/offers/{offer_id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addProductToCategory(@PathVariable("id") Long id, @PathVariable("offer_id") Long offer ) {
        final Category category = getCategoryById(id);
        final Offer offerEntity = offerService.findOffer(offer);
        categoryService.addOffer(category, offerEntity);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_GUEST')")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Category getCategoryById(@PathVariable("id") Long id) {
        final Category category = categoryService.findCategory(id);
        if (category == null) {
            throw new NoResultException("Category with id " + id + " was not found.");
        }
        return category;
    }
}
