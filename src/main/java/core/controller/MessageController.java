package core.controller;


import core.model.Message;

import core.security.model.AuthenticationToken;
import core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/messages")
public class MessageController {

    @Autowired
    private UserService userService;

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    @PostMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void sendMessage(Principal principal, @RequestBody Message message, @PathVariable("id") Long user_id) {
        final AuthenticationToken auth = (AuthenticationToken) principal;
        message.setSender(auth.getPrincipal().getUser());
        message.setReceiver(userService.findUser(user_id));
        userService.sendMessage(message);
    }
}
