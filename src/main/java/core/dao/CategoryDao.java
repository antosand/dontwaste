package core.dao;

import core.model.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class CategoryDao extends AbstractDao<Category> {

    public CategoryDao() {
        super(Category.class);
    }

    public Category findByName(String categoryName) {
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<Category> cq = qb.createQuery(Category.class);
        Root<Category> r = cq.from(Category.class);
        cq.where(qb.equal(r.get("name"), categoryName));
        return getSingleResultOrNull(em.createQuery(cq));
    }
}
