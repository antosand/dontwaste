package core.dao;

import core.model.Message;
import core.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class MessageDao extends AbstractDao<Message> {

    public MessageDao() {
        super(Message.class);
    }

    public List<Message> findBySender(User user) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Message> cq = cb.createQuery(Message.class);
        Root<Message> r = cq.from(Message.class);
        cq.where(cb.equal(r.get("sender"), user));
        return em.createQuery(cq).getResultList();
    }
}
