package core.dao;

import core.model.Comment;
import core.model.Offer;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class CommentDao extends AbstractDao<Comment> {

    public CommentDao() {
        super(Comment.class);
    }

    public List<Comment> getComments(Offer offer) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Comment> cq = cb.createQuery(Comment.class);
        Root<Comment> r = cq.from(Comment.class);
        cq.where(cb.equal(r.get("offer"), offer));
        return em.createQuery(cq).getResultList();
    }
}
