package core.dao;

import core.model.Address;
import org.springframework.stereotype.Repository;

@Repository
public class AddressDao extends AbstractDao<Address> {
    public AddressDao() {
        super(Address.class);
    }
}
