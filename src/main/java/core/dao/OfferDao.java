package core.dao;

import core.model.Category;
import core.model.Offer;
import core.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class OfferDao extends AbstractDao<Offer> {

    public OfferDao() {
        super(Offer.class);
    }

    public Offer findByName(String name) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Offer> cq = cb.createQuery(Offer.class);
        Root<Offer> r = cq.from(Offer.class);
        cq.where(cb.equal(r.get("name"), name));
        return getSingleResultOrNull(em.createQuery(cq));
    }

    public List<Offer> findOffersByCategory(Category category) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Offer> cq = cb.createQuery(Offer.class);
        Root<Offer> r = cq.from(Offer.class);
        cq.where(cb.isMember(category, r.<List>get("categories")));
        return em.createQuery(cq).getResultList();
    }

    public List<Offer> findOffersByUser(User user) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Offer> cq = cb.createQuery(Offer.class);
        Root<Offer> r = cq.from(Offer.class);
        cq.where(cb.equal(r.get("createdBy"), user));
        return em.createQuery(cq).getResultList();
    }
}
