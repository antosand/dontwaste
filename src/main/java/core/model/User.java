package core.model;

import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity(name = "sys_user")
public class User extends AbstractEntity {

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String username;

    @Column
    private String password;

    @Enumerated(EnumType.STRING)
    private Role role = Role.GUEST;

//    @OneToMany(mappedBy = "receiver", cascade = CascadeType.ALL)
//    private List<Message> receivedMessages;
//
//    @OneToMany(mappedBy = "sender", cascade = CascadeType.ALL)
//    private List<Message> sentMessages;

    private Address address;

    public String getFirstName() {
        return firstName;
    }

    public void erasePassword() {
        this.password = null;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void encodePassword(PasswordEncoder encoder) {
        this.password = encoder.encode(password);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

//    public List<Message> getReceivedMessages() {
//        return receivedMessages;
//    }
//
//    public void setReceivedMessages(List<Message> receivedMessages) {
//        this.receivedMessages = receivedMessages;
//    }
//
//    public List<Message> getSentMessages() {
//        return sentMessages;
//    }
//
//    public void setSentMessages(List<Message> sentMessages) {
//        this.sentMessages = sentMessages;
//    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
