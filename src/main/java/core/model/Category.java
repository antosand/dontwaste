package core.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Category extends AbstractEntity {

    private String name;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Offer> offers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void addOffer(Offer offer) {
        offers.add(offer);
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }
}
