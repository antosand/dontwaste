package core.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Address extends AbstractEntity {

    @Column(nullable = false)
    private String city;

    @Column(nullable = true)
    private String street;

    @Column(nullable = true)
    private String building;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }
}
