package core.service;

import core.dao.CommentDao;
import core.dao.OfferDao;
import core.model.Comment;
import core.model.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    private CommentDao commentDao;

    @Autowired
    private OfferDao offerDao;

    @Transactional
    public List<Comment> findAll(Long offerId) {
        return commentDao.getComments(offerDao.find(offerId));
    }
}
