package core.service;

import core.dao.CommentDao;
import core.dao.OfferDao;
import core.model.Comment;
import core.model.Offer;
import core.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class OfferService {

    private CommentDao comDao;
    private OfferDao offDao;

    @Autowired
    public OfferService(OfferDao offDao, CommentDao comDao) {
        this.offDao = offDao;
        this.comDao = comDao;
    }

    @Transactional
    public Offer findOffer(Long id) {
        return offDao.find(id);
    }

    @Transactional
    public void addOffer(Offer offer) {
        offer.setCreatedOn(new Date());
        offDao.create(offer);
    }

    @Transactional
    public void removeOffer(Offer offer) {
        offDao.remove(offer);
    }

    @Transactional
    public List<Offer> findAll() {
        return offDao.findAll();
    }

//    public List<Comment> getComments() {
//        if (offer == null) return null;
//        return comDao.getComments(offer);
//    }

    @Transactional
    public void addComment(User user, Comment comment) {
        comment.setOffer(comment.getOffer());
        comment.setOwner(user);
        comment.setContent(comment.getContent());
        comDao.create(comment);
    }
}
