package core.service;

import core.dao.MessageDao;
import core.dao.UserDao;
import core.model.Message;
import core.model.Role;
import core.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    private final UserDao dao;
    private final MessageDao mDao;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserDao dao, MessageDao mDao, PasswordEncoder passwordEncoder) {
        this.dao = dao;
        this.mDao = mDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public void addUser(User user) {
        Objects.requireNonNull(user);
        if (user.getRole() == Role.GUEST || user.getRole() == null) {
            user.setRole(Role.USER);
        }
        user.encodePassword(passwordEncoder);
        dao.create(user);
    }

    @Transactional
    public List<Message> getMessages(User user) {
        return mDao.findBySender(user);
    }

    @Transactional
    public void sendMessage(Message message) {
        mDao.create(message);
    }

    @Transactional
    public User findUser(Long user_id) {
        return dao.find(user_id);
    }

    @Transactional
    public void setAdmin(User u) {
        u.setRole(Role.ADMIN);
        u.setPassword(dao.find(u.getId()).getPassword() );
        dao.edit(u);
    }
}
