package core.service;

import core.dao.CategoryDao;
import core.dao.OfferDao;
import core.model.Category;
import core.model.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class CategoryService {

    private final CategoryDao dao;
    private final OfferDao offerDao;

    @Autowired
    public CategoryService(CategoryDao dao, OfferDao offerDao) {
        this.dao = dao;
        this.offerDao = offerDao;
    }

    @Transactional
    public void addCategory(Category category) {
        Objects.requireNonNull(category);
        dao.create(category);
    }

    @Transactional(readOnly = true)
    public List<Offer> findAllOffersByCategory(String name) {
        return offerDao.findOffersByCategory(dao.findByName(name));
    }

    @Transactional(readOnly = true)
    public Category findCategory(Long id) {
        return dao.find(id);
    }

    @Transactional(readOnly = true)
    public List<Category> findAll() {
        return dao.findAll();
    }

    @Transactional
    public void createCategory(Category category) {
        dao.create(category);
    }

    @Transactional
    public void addOffer(Category category, Offer offer) {
        offer.addCategory(category);
        offerDao.edit(offer);
        category.addOffer(offer);
        dao.edit(category);
    }

    @Transactional(readOnly = true)
    public void removeCategory(Category toRemove) {
        dao.remove(toRemove);
    }
}
