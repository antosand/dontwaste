package core.dao;

import core.model.Category;
import core.model.Offer;
import core.model.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class OfferDaoTest extends AbstractDaoTestRunner {

    @Autowired
    private OfferDao offDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CategoryDao categoryDao;

    @Test
    public void offerCRUD() {
        Offer emptyOffer = new Offer();
        offDao.create(emptyOffer);
        assertNotNull(offDao.find(emptyOffer.getId()));
        offDao.remove(emptyOffer);
        assertNull(offDao.find(emptyOffer.getId()));
    }

    @Test
    public void findByName_findByUserWhoAdded() {
        Offer offer = new Offer();
        Offer offer2 = new Offer();
        Offer offer3 = new Offer();
        User user = new User();
        user.setUsername("Username");
        user.setFirstName("FirstName");
        user.setLastName("LastName");
        user.setPassword("Password");
        offer.setName("Offer");

        userDao.create(user);
        offer.setCreatedBy(user);
        offer2.setCreatedBy(user);
        offer3.setCreatedBy(user);

        offDao.create(offer);
        offDao.create(offer2);
        offDao.create(offer3);

        assertEquals(offer, offDao.findByName("Offer"));

        assertEquals(3, offDao.findOffersByUser(user).size());
    }

    @Test
    public void findOffersByCategorySuccess() {
        Category category = new Category();
        category.setName("Cars");
        List<Offer> offers = new ArrayList<>();

        Offer bmw = new Offer();
        bmw.setName("BMW");
//        bmw.addCategory(category);

        Offer fiat = new Offer();
        fiat.setName("Fiat");
//        fiat.addCategory(category);

        offers.add(bmw);
        offers.add(fiat);

        category.setOffers(offers);
        categoryDao.create(category);

        offDao.create(bmw);
        offDao.create(fiat);

        assertNotNull(offDao.findByName("BMW"));
        assertNotNull(offDao.findByName("Fiat"));

        List<Offer> result = offDao.findOffersByCategory(category);
        assertEquals(result.size(), 2);
    }
}
