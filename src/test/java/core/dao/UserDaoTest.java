package core.dao;

import core.model.Role;
import core.model.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

public class UserDaoTest extends AbstractDaoTestRunner {

    private Random random = new Random();
    private int randInt;

    @Before
    public void init() {
        randInt = random.nextInt();
    }

    @Autowired
    private UserDao dao;

    @Test
    public void addUser_findByUsername_editUser_findByRole_deleteUser() {
        User testUser = new User();
        testUser.setFirstName("FirstName" + randInt);
        testUser.setLastName("LastName" + randInt);
        testUser.setUsername("Username" + randInt);
        testUser.setPassword(String.valueOf(randInt));
        testUser.setRole(Role.USER);
        dao.create(testUser);

        User foundByUsername = dao.findByUsername(testUser.getUsername());
        assertEquals(testUser, foundByUsername);

        List<User> foundByRole = dao.findAllByRole(testUser.getRole());
        assertTrue(foundByRole.contains(testUser));

        dao.remove(testUser);
        assertNull(dao.findByUsername(testUser.getUsername()));
    }


}
