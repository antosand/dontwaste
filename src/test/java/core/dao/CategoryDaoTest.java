package core.dao;

import core.model.Category;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.Assert.*;

import javax.persistence.PersistenceContext;

public class CategoryDaoTest extends AbstractDaoTestRunner {

    @Autowired
    private CategoryDao dao;

    @Test
    public void findByCategoryNameReturnsMatchingName() {
        Category category = new Category();
        category.setName("Products");
        dao.create(category);

        final Category result = dao.findByName(category.getName());
        assertNotNull(result);
        assertEquals(category.getId(), result.getId());
    }

    @Test
    public void findBycategoryNameFailsMatching() {
        Category category = new Category();
        category.setName("Failed");
        dao.create(category);

        final Category result = dao.findByName("Products");
        assertNull(result);
    }
}
