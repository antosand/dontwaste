package core.service;

import core.model.Category;
import core.model.Offer;
import core.model.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.*;

public class OfferServiceTest extends AbstractServiceTestRunner {

    @Autowired
    private OfferService offSer;

    @Autowired
    private CategoryService catSer;

    @Test
    public void addCommentsToOffer_findAllComments_deleteComments() {
//        User testUser = new User();
//        testUser.setFirstName("FirstName");
//        testUser.setLastName("LastName");
//        testUser.setUsername("Username");
//        testUser.setPassword("Password");
//
//        Offer offer = new Offer();
//        offSer.setOffer(offer);
//
//        for (int i = 0; i < 20; i++) {
//            offSer.addComment(testUser, "Comment: " + i);
//        }
//        assertEquals(20, offSer.getComments().size());
    }

    @Test
    public void addOfferToCategory_findOffer() {
        Offer offer = new Offer();
        offer.setCreatedOn(new Date());
        offer.setName("Apple");
        offSer.addOffer(offer);
        Category category = new Category();
        category.setName("Food");
        catSer.createCategory(category);
        catSer.addOffer(category, offer);

        assertEquals(category, offer.getCategories().get(0));
        assertEquals(offer, category.getOffers().get(0));
    }
}
