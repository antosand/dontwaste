package core.service;

import core.dao.MessageDao;
import core.dao.UserDao;
import core.environment.Generator;
import core.model.Role;
import core.model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Random;

import static org.junit.Assert.*;

public class UserServiceTest extends AbstractServiceTestRunner {

    private Random random = new Random();
    private int randInt;
    private User testUser;
    private User testUser2;

    @Before
    public void init() {
        randInt = random.nextInt();
    }

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private UserDao dao;

    @Autowired
    private MessageDao mDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService us;

    @Before
    public void createUsers() {
        testUser = new User();
        testUser.setFirstName("FirstName" + randInt);
        testUser.setLastName("LastName" + randInt);
        testUser.setUsername("Username" + randInt);
        testUser.setPassword(String.valueOf(randInt));
        us.addUser(testUser);

        testUser2 = new User();
        testUser2.setFirstName("FirstName" + randInt + 1);
        testUser2.setLastName("LastName" + randInt + 1);
        testUser2.setUsername("Username" + randInt + 1);
        testUser2.setPassword(String.valueOf(randInt + 1));
        us.addUser(testUser2);
    }

    @After
    public void removeUsers() {
        if (testUser != null) dao.remove(testUser);
        if (testUser2 != null) dao.remove(testUser2);
    }

    @Test
    public void findUser() {
        User result = em.find(User.class, testUser.getId());
        assertEquals(testUser, result);
        assertNotEquals(testUser2, result);
    }

//    @Test
//    public void sendMessages_findMessage() {
//        for (int i = 0; i < 20; i++) {
//            us.sendMessage("Hello world" + i, testUser, testUser2);
//        }
//        assertEquals(20, us.getMessages(testUser).size());
//    }

    @Test
    public void persistEncodesUserPassword() {
        final User user = Generator.generateUser();
        final String rawPassword = user.getPassword();
        us.addUser(user);

        final User result = em.find(User.class, user.getId());
        assertNotNull(result);
        assertTrue(passwordEncoder.matches(rawPassword, result.getPassword()));
    }

    @Test
    public void persistSetsUserRoleToDefaultWhenItIsNotSpecified() {
        final User user = Generator.generateUser();
        user.setRole(null);
        us.addUser(user);

        final User result = em.find(User.class, user.getId());
        assertEquals(Role.USER, result.getRole());
    }
}
